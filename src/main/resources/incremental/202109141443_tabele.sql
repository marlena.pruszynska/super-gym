--liquibase formatted sql
--changeset zsi:202108301605_dodatkowe_tabele


CREATE TABLE clients
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE card_type
(
    id SERIAL PRIMARY KEY,
    name  VARCHAR(50),
    price int
);

CREATE TABLE gym
(
    id   SERIAL  PRIMARY KEY,
    name      VARCHAR(50),
    city      VARCHAR(50),
    open_from VARCHAR(50),
    open_to   VARCHAR(50)

);

CREATE TABLE employee
(
    id SERIAL PRIMARY KEY,
    gym_id int,
    name   VARCHAR(200),
    type   VARCHAR(50),
    CONSTRAINT fk_employee_gym
        FOREIGN KEY (gym_id)
            REFERENCES gym (id)
);

CREATE TABLE gym_card_type
(
    id     SERIAL PRIMARY KEY,
    gym_id int,
    card_type_id int,
    CONSTRAINT fk_gym_type
        FOREIGN KEY (card_type_id)
            REFERENCES card_type (id),
    CONSTRAINT gym_card_type_gym
        FOREIGN KEY (gym_id)
            REFERENCES gym (id)

);

CREATE TABLE card
(
    id SERIAL PRIMARY KEY,
    card_type_id int,
    client_id int,
    valid_from   VARCHAR(50),
    valid_to     VARCHAR(50),
    CONSTRAINT fk_card_card
        FOREIGN KEY (card_type_id)
            REFERENCES card_type (id),
    CONSTRAINT fk_card_clients
FOREIGN KEY (client_id)
REFERENCES clients(id)
);
