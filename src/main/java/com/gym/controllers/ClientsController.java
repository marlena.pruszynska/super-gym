package com.gym.controllers;

import com.gym.DTO.ClientDTO;
import com.gym.DTO.CreateClientDTO;
import com.gym.services.ClientsService;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class ClientsController {
    private final ClientsService clientsService;

    @GetMapping("/clients/{id}")
    public ClientDTO getClients(@PathVariable Long id){return clientsService.getClients(id);}
    @PostMapping("/clients")
    public ClientDTO createClient(@RequestBody CreateClientDTO createClientDTO){
        return clientsService.createClient(createClientDTO);}
    @DeleteMapping("/clients/{id}")
    public void deleteClient(@PathVariable Long id){clientsService.deleteClients(id);}
}
