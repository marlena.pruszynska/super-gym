package com.gym.controllers;

import com.gym.DTO.CreateGymCardTypeDTO;
import com.gym.DTO.GymCardTypeDTO;
import com.gym.services.GymCardTypeService;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class GymCardTypeController {
    private final GymCardTypeService gymCardTypeService;

    @GetMapping("/gym-card-type/{id}")
    public GymCardTypeDTO getGymCardType(@PathVariable Long id){return gymCardTypeService.getGymCardType(id);}
    @PostMapping("/gym-card-type")
    public GymCardTypeDTO createGymCardType(@RequestBody CreateGymCardTypeDTO createGymCardTypeDTO ){
        return gymCardTypeService.createGymCardType(createGymCardTypeDTO);}
    @DeleteMapping("/gym-card-type/{id}")
    public void deleteGymCrdType(@PathVariable Long id){
        gymCardTypeService.deleteGymCardType(id);}


}
