package com.gym.controllers;

import com.gym.DTO.CardTypeDTO;
import com.gym.DTO.CreateCardTypeDTO;
import com.gym.services.CardTypeService;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class CardTypeController {
    private final CardTypeService cardTypeService;

    @GetMapping("/card-type/{id}")
    public CardTypeDTO getCardType(@PathVariable Long id) {
        return cardTypeService.getCardType(id);
    }

    @PostMapping("/card-type")
    public CardTypeDTO createCardType(@RequestBody CreateCardTypeDTO createCardTypeDTO) {
        return cardTypeService.createCardType(createCardTypeDTO);
    }

    @DeleteMapping("/card-type/{id}")
    public void deleteCardType(@PathVariable Long id) {
        cardTypeService.deleteCardType(id);
    }
}
