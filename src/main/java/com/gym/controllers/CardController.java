package com.gym.controllers;

import com.gym.DTO.CardDTO;

import com.gym.DTO.CreateCardDTO;
import com.gym.services.CardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class CardController {
    private final CardService cardService;

    @GetMapping("/cards/{id}")
    public CardDTO getCard(@PathVariable Long id) {
        return cardService.getCard(id);
    }
    @PostMapping("/cards")
    public CardDTO createCard(@RequestBody CreateCardDTO createCardDTO){
        return cardService.createCard(createCardDTO);
    }
    @DeleteMapping("/cards/{id}")
    public  void deleteCard(@PathVariable Long id){
        cardService.deleteCard(id);
    }

}
