package com.gym.controllers;

import com.gym.DTO.CreateGymDTO;
import com.gym.DTO.GymDTO;
import com.gym.services.GymService;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
public class GymController {
    private final GymService gymService;

    @GetMapping("/gym/{id}")
    public GymDTO getGym(@PathVariable Long id){
        return gymService.getGym(id);
    }
    @PostMapping("/gym")
    public GymDTO createGym(@RequestBody CreateGymDTO createGymDTO){
        return gymService.createGym(createGymDTO);
    }
    @DeleteMapping("/gym/{id}")
    public void deleteGym(@PathVariable Long id){
        gymService.deleteGym(id);
    }
}
