package com.gym.services;

import com.gym.DTO.CardTypeDTO;
import com.gym.DTO.CreateGymCardTypeDTO;
import com.gym.DTO.GymCardTypeDTO;
import com.gym.DTO.GymDTO;
import com.gym.entities.CardType;
import com.gym.entities.Gym;
import com.gym.entities.GymCardType;
import com.gym.respositories.CardTypeRepository;
import com.gym.respositories.GymCardTypeRepository;
import com.gym.respositories.GymRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GymCardTypeService {
    private final GymCardTypeRepository gymCardTypeRepository;
    private final GymRepository gymRepository;
    private final CardTypeRepository cardTypeRepository;
    public GymCardTypeDTO getGymCardType(Long id) {
        Optional<GymCardType> gymCardType = gymCardTypeRepository.findById(id);
        if (gymCardType.isEmpty()) {
            throw new IllegalStateException("nie ma takiego rodzaju karty na siłowni");
        }
        GymCardTypeDTO gymCardTypeDTO = GymCardTypeDTO
                .builder()
                .id(gymCardType.get().getId())
                .gym(mapToGymDto(gymCardType.get().getGym()))
                .cardType(mapToCardType(gymCardType.get().getCardType()))
                .build();
        return gymCardTypeDTO;
    }

    private CardTypeDTO mapToCardType(CardType cardType) {
        return CardTypeDTO
                .builder()
                .id(cardType.getId())
                .name(cardType.getName())
                .price(cardType.getPrice())
                .build();
    }

    private GymDTO mapToGymDto(Gym gym) {
        return GymDTO
                .builder()
                .id(gym.getId())
                .name(gym.getName())
                .city(gym.getCity())
                .openFrom(gym.getOpenFrom())
                .openTo(gym.getOpenTo())
                .build();
    }

    public GymCardTypeDTO createGymCardType(CreateGymCardTypeDTO createGymCardTypeDTO) {
        final Gym foundGym = gymRepository.findById(createGymCardTypeDTO.getGymId()).orElseThrow(() -> {
            throw new IllegalStateException(String.format("Nie ma silowni o ID: %d", createGymCardTypeDTO.getGymId()));
        });
        final  CardType foundCardtype= cardTypeRepository.findById(createGymCardTypeDTO.getCardTypeId()).orElseThrow(()->{throw new IllegalStateException(String.format("Nie ma typu carty o id %d", createGymCardTypeDTO.getCardTypeId()));});
        GymCardType gymCardType = new GymCardType();
        gymCardType.setGym(foundGym);
        gymCardType.setCardType(foundCardtype);
        GymCardType savedGymCardType = gymCardTypeRepository.save(gymCardType);

        GymCardTypeDTO gymCardTypeDTO=new GymCardTypeDTO();
        gymCardTypeDTO.setId(savedGymCardType.getId());
        gymCardTypeDTO.setGym(mapToGymDto(savedGymCardType.getGym()));
        gymCardTypeDTO.setCardType(mapToCardType(savedGymCardType.getCardType()));
        return gymCardTypeDTO;
    }


    public void deleteGymCardType(Long id) {
        gymCardTypeRepository.deleteById(id);
    }
}
