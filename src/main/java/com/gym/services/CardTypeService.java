package com.gym.services;

import com.gym.DTO.CardTypeDTO;
import com.gym.DTO.CreateCardTypeDTO;
import com.gym.entities.CardType;
import com.gym.respositories.CardTypeRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CardTypeService {
    private final CardTypeRepository cardTypeRepository;

    public CardTypeDTO getCardType(Long id) {
        Optional<CardType> cardTypes = cardTypeRepository.findById(id);
        if(cardTypes.isEmpty()){
            throw new IllegalStateException("Nie ma karty takie typu");
        }
        CardTypeDTO cardTypeDTO= CardTypeDTO
                .builder()
                .id(cardTypes.get().getId())
                .name(cardTypes.get().getName())
                .price(cardTypes.get().getPrice())
                .build();
        return cardTypeDTO;
    }

    public CardTypeDTO createCardType(CreateCardTypeDTO createCardTypeDTO) {
        CardType cardType=new CardType();
        cardType.setName(createCardTypeDTO.getName());
        cardType.setPrice(createCardTypeDTO.getPrice());
        CardType savedCardType = cardTypeRepository.save(cardType);

        CardTypeDTO cardTypeDTO=new CardTypeDTO();
        cardTypeDTO.setId(savedCardType.getId());
        cardTypeDTO.setName(savedCardType.getName());
        cardTypeDTO.setPrice(savedCardType.getPrice());
        return cardTypeDTO;
    }

    public void deleteCardType(Long id) {
        cardTypeRepository.deleteById(id);
    }
}
