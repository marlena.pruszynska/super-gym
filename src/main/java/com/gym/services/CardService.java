package com.gym.services;

import com.gym.DTO.CardDTO;
import com.gym.DTO.CardTypeDTO;
import com.gym.DTO.CreateCardDTO;
import com.gym.entities.Card;
import com.gym.entities.CardType;
import com.gym.entities.Client;
import com.gym.respositories.CardRepository;
import com.gym.respositories.CardTypeRepository;
import com.gym.respositories.ClientsRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CardService {
    private final CardRepository cardRepository;
    private final ClientsRepository clientRepository;
    private final CardTypeRepository cardTypeRepository;

    public CardDTO getCard(Long id) {
        Optional<Card> card = cardRepository.findById(id);
        if(card.isEmpty()){
            throw new IllegalStateException("Nie ma takiej karty");
        }
        CardDTO cardDTO=CardDTO
                .builder()
                .id(card.get().getId())
                .validFrom(card.get().getValidFrom())
                .validTo(card.get().getValidTo())
                .build();
        return cardDTO;
    }

    public CardDTO createCard(CreateCardDTO createCardDTO) {
        Client client = clientRepository.findById(createCardDTO.getClientId()).orElseThrow(() -> {
            throw new IllegalStateException("Nie ma takiego klienta");
        });
        CardType cardType= cardTypeRepository.findById(createCardDTO.getCardTypeId()).orElseThrow(()->{
            throw new IllegalStateException("Nie ma karty o takim typie");
            });

        Card card=new Card();
        card.setValidFrom(createCardDTO.getValidFrom());
        card.setValidTo(createCardDTO.getValidTo());
        card.setClient(client);
        card.setCardType(cardType);
        Card savedCard =cardRepository.save(card);

        CardDTO cardDTO=new CardDTO();
        cardDTO.setId(savedCard.getId());
        cardDTO.setValidFrom(savedCard.getValidFrom());
        cardDTO.setValidTo(savedCard.getValidTo());
        cardDTO.setCardType(mapCardType(cardType));

        return cardDTO;
    }

    public static CardTypeDTO mapCardType(CardType cardType) {
       return CardTypeDTO
                .builder()
                .id(cardType.getId())
                .name(cardType.getName())
                .price(cardType.getPrice())
                .build();
    }


    public void deleteCard(Long id) {
        cardRepository.deleteById(id);
    }


}
