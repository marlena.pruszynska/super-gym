package com.gym.services;

import com.gym.DTO.CreateEmployeeDTO;
import com.gym.DTO.EmployeeDTO;
import com.gym.entities.Employee;
import com.gym.respositories.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeDTO getEmployee(Long id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isEmpty()) {
            throw new IllegalStateException("Nie ma takie pracownika");
        }
        EmployeeDTO employeeDTO = EmployeeDTO
                .builder()
                .id(employee.get().getId())
                .name(employee.get().getName())
                .type(employee.get().getType())
                .build();
        return employeeDTO;
    }

    public EmployeeDTO createEmployee(CreateEmployeeDTO createEmployeeDTO) {
        Employee employee = new Employee();
        employee.setName(createEmployeeDTO.getName());
        employee.setType(createEmployeeDTO.getType());
        Employee savedEmployee = employeeRepository.save(employee);

        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setId(savedEmployee.getId());
        employeeDTO.setName(savedEmployee.getName());
        employeeDTO.setType(savedEmployee.getType());
        return employeeDTO;
    }


    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}
