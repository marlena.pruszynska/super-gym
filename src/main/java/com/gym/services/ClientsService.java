package com.gym.services;

import com.gym.DTO.CardDTO;
import com.gym.DTO.ClientDTO;
import com.gym.DTO.CreateClientDTO;
import com.gym.entities.Card;
import com.gym.entities.Client;
import com.gym.respositories.ClientsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClientsService {
    private final ClientsRepository clientsRepository;

    public ClientDTO getClients(Long id) {
        Optional<Client> clients = clientsRepository.findById(id);
        if (clients.isEmpty()){
            throw new IllegalStateException("Nie ma takiego klienta");
        }
        ClientDTO clientsDTO = ClientDTO
                .builder()
                .id(clients.get().getId())
                .name(clients.get().getName())
                .cards(mapCardsToDto(clients.get().getCards()))
                .build();
        return clientsDTO;
    }


    private Set<CardDTO> mapCardsToDto(Set<Card> cards) {
       return cards
                .stream()
                .map(card -> {
                    CardDTO cardDTO=new CardDTO();
                    cardDTO.setValidFrom(card.getValidFrom());
                    cardDTO.setValidTo(card.getValidTo());
                    cardDTO.setCardType(CardService.mapCardType(card.getCardType()));
                    return cardDTO;
                }).collect(Collectors.toSet());
    }

    public ClientDTO createClient(CreateClientDTO createClientDTO) {
        Client client= new Client();
        client.setName(createClientDTO.getName());
        Client savedClient= clientsRepository.save(client);

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(savedClient.getId());
        clientDTO.setName(savedClient.getName());

        return clientDTO;
    }

    public void deleteClients(Long id) {
        clientsRepository.deleteById(id);
    }
}
