package com.gym.services;

import com.gym.DTO.CreateGymDTO;
import com.gym.DTO.GymDTO;
import com.gym.entities.Gym;
import com.gym.respositories.GymRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GymService {
    private final GymRepository gymRepository;

    public GymDTO getGym(Long id) {
        Optional<Gym> gym=gymRepository.findById(id);
        if(gym.isEmpty()){
            throw new IllegalStateException("Nie ma takiej siłowni");
        }
        return GymDTO
                .builder()
                .id(gym.get().getId())
                .name(gym.get().getName())
                .city(gym.get().getCity())
                .openFrom(gym.get().getOpenFrom())
                .openTo(gym.get().getOpenTo())
                .build();
    }

    public GymDTO createGym(CreateGymDTO createGymDTO) {
        Gym gym=new Gym();
        gym.setName(createGymDTO.getName());
        gym.setCity(createGymDTO.getCity());
        gym.setOpenFrom(createGymDTO.getOpenFrom());
        gym.setOpenTo(createGymDTO.getOpenTo());
        Gym savedGym=gymRepository.save(gym);

        GymDTO gymDTO=new GymDTO();
        gymDTO.setId(savedGym.getId());
        gymDTO.setName(savedGym.getName());
        gym.setCity(savedGym.getCity());
        gym.setOpenFrom(savedGym.getOpenFrom());
        gym.setOpenTo(savedGym.getOpenTo());
        return gymDTO;

    }

    public void deleteGym(Long id) {
        gymRepository.deleteById(id);
    }
}
