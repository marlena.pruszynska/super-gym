package com.gym.DTO;


import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCardDTO {

    private Long cardTypeId;
    private Long clientId;
    private String validFrom;
    private String validTo;
}

