package com.gym.DTO;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GymDTO {
    private Long id;
    private String name;
    private String city;
    private String openFrom;
    private String openTo;
}
