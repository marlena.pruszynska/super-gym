package com.gym.DTO;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GymCardTypeDTO {
    private Long id;
    private GymDTO gym;
    private CardTypeDTO cardType;
}
