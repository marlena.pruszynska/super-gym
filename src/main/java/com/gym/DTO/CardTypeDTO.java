package com.gym.DTO;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CardTypeDTO {
    private Long id;
    private String name;
    private int price;
}
