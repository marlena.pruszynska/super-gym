package com.gym.DTO;

import lombok.*;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class CreateGymCardTypeDTO {
    private Long id;
    private Long gymId;
    private Long cardTypeId;
}
