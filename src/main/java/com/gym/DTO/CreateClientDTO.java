package com.gym.DTO;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CreateClientDTO {
    private String name;
}
