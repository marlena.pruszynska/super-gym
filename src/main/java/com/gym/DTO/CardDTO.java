package com.gym.DTO;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CardDTO {

    private Long id;
    private CardTypeDTO cardType;
    private String validFrom;
    private String validTo;
}
