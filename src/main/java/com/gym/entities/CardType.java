package com.gym.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "card_type")
public class CardType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int price;
    @OneToMany(mappedBy = "cardType", orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Card> cards;
    @OneToMany(mappedBy = "cardType", orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<GymCardType> gymCardTypes;
}
