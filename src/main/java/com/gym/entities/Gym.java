package com.gym.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="gym")
public class Gym {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String city;
    private String openFrom;
    private String openTo;
    @OneToMany(orphanRemoval = true, mappedBy = "gym",cascade = CascadeType.ALL)
    private Set<GymCardType> gymCardTypes;
    @OneToMany(orphanRemoval = true,mappedBy = "gym",cascade = CascadeType.ALL)
    private Set<Employee> employees;
}
