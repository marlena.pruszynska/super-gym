package com.gym.respositories;

import com.gym.entities.Card;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository <Card, Long> {
}
