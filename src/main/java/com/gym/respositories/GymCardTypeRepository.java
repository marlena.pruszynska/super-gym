package com.gym.respositories;

import com.gym.entities.GymCardType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GymCardTypeRepository extends JpaRepository <GymCardType, Long> {
}
