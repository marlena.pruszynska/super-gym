package com.gym.respositories;

import com.gym.entities.CardType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardTypeRepository extends JpaRepository<CardType, Long> {
}
